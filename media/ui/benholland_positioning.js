var colCount = 4;
var colWidth = 0;
var margin = 15;
var windowWidth = 0;
var offset = 222;
var extraSpace = 0;
var blocks = [];
var marginTop = 65;
var minWindowWidth = 400;
var windowWidthFix = 40;

$(function(){
	$(window).resize(setupBlocks);
});

function setupBlocks() {
	windowWidth = $(window).width() - offset + windowWidthFix;
    if (windowWidth < minWindowWidth) {
        return;
    }
	colWidth = $('.div_notice_card').outerWidth();
	blocks = [];
	console.log(blocks);
	colCount = Math.floor(windowWidth/(colWidth+margin*2));
    extraSpace = windowWidth-colCount*(colWidth+margin*2);
	for(var i=0;i<colCount;i++){
		blocks.push(marginTop);
	}
	positionBlocks();
}

function positionBlocks() {
	$('.div_notice_card').each(function(){
		var min = Array.min(blocks);
		var index = $.inArray(min, blocks);
		var leftPos = margin+(index*(colWidth+margin));
		$(this).css({
			'left':(offset+leftPos+(colCount>=2?(extraSpace/colCount):0))+'px',
			'top':min+'px'
		});
		blocks[index] = min+$(this).outerHeight()+margin;
	});
    var maxHeight = Array.max(blocks);
    if ($('#div_filter_bar').height() < maxHeight) {
        $('#div_filter_bar').css({
            'height': maxHeight
        });
    }
}

// Function to get the Min value in Array
Array.min = function(array) {
    return Math.min.apply(Math, array);
};
// Function to get the Max value in Array
Array.max = function(array) {
    return Math.max.apply(Math, array);
};