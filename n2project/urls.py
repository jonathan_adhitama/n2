from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

# Moderator and Admin
from n2.moderator_site import moderator_site

# Dajaxice (1/2)
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    url(r'^admin/', include(moderator_site.urls)),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
    url(r'', include('n2.urls', namespace='n2')),
)

# Dajaxice (2/2)
urlpatterns += staticfiles_urlpatterns()

handler403 = 'n2.views.page403'
handler404 = 'n2.views.page404'
handler500 = 'n2.views.page500'