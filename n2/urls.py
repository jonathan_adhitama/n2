from django.conf.urls import patterns, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from n2 import views
from n2project import settings

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login$', views.vlogin, name='login'),
    url(r'^logout$', views.vlogout, name='logout'),
    url(r'^register$', views.vregister, name='register'),
    url(r'^activate/(?P<user_pk>\d+)/(?P<activation_key>\w+)', views.activate_user, name='activate'),
    url(r'^notice/(?P<notice_id>\d+)$', views.notice, name='notice'),
    url(r'^compose$', views.compose, name='compose_new'),
    url(r'^compose/(?P<notice_id>\d+)$', views.compose, name='compose_edit'),
    url(r'^profile$', views.edit_user, name='edit_user'),
    url(r'^reset$', views.reset_password, name='reset'),
    url(r'^about$', views.about, name='about'),
    url(r'^register_complete$', views.register_complete, name='register_complete'),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)

urlpatterns += staticfiles_urlpatterns()