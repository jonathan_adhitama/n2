def notice_search_keywords(notices_range, query_keywords):
    if len(query_keywords) == 0:
        return set(notices_range)
    keywords = [qk.strip() for qk in query_keywords.split()]
    matches = set()
    for keyword in keywords:
        matches |= set(notices_range.filter(title__icontains=keyword))
        matches |= set(notices_range.filter(content__icontains=keyword))
        matches |= set(notices_range.filter(author__username__icontains=keyword))
        matches |= set(notices_range.all().filter(tags__tagName__icontains=keyword))
    return matches

def notice_search_tags(tags_range, notices_range, tag_pks):

    if len(tag_pks) == 0:
        return set(notices_range)
    matches = set()
    for tag_pk in tag_pks:
        matches |= set(tags_range.get(pk=tag_pk).notice_set.all())
    return matches

def get_notice_date_published(notice):
    return notice.datePublished

def get_notice_num_views(notice):
    return len(notice.viewedBy.all())

def get_notice_num_likes(notice):
    return len(notice.likedBy.all())

def notice_search(notices_range, tags_range, query_keywords, tag_pks, sort_by, reverse):
    # notices_range is a queryset of Notice objects such as Notice.objects.all()
    # tags_range is a queryset of Tag objects such as Tag.objects.all()
    # query_keywords is a list of strings
    # tag_pks is a list of ints

    tag_matches = notice_search_tags(tags_range, notices_range, tag_pks)
    keyword_matches = notice_search_keywords(notices_range, query_keywords)

    final_matches = tag_matches.intersection(keyword_matches)

    sort_key_functions = {
        'date_published': get_notice_date_published,
        'num_views': get_notice_num_views,
        'num_likes': get_notice_num_likes,
    }

    return sorted(final_matches, key=sort_key_functions[sort_by], reverse=reverse)


