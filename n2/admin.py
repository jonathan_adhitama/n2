import django
from django.contrib import admin
from django.contrib.auth.models import Permission
from django.db.models import Q

from n2.models import TagGroup, Tag, Notice, N2User
from n2.forms import admin_notice_form


def is_moderator(user):
    return (user.groups.filter(name='moderator').exists() or is_administrator(user))

def is_administrator(user):
    return (user.groups.filter(name='admin').exists() or user.is_superuser)

def is_superuser(user):
    return user.is_superuser


class TagGroupAdmin(admin.ModelAdmin):
    list_per_page = 30
    
    fieldsets = [
        (None, {
            'classes': ('wide',),
            'fields': ['tagGroupName']
        })
    ]

class TagAdmin(admin.ModelAdmin):
    list_display = ('tagName', 'tagGroup', 'status')
    list_filter = ['status', 'tagGroup']
    list_per_page = 30
    actions = ['mark_approved', 'mark_disapproved']
    search_fields = ['tagName',]
    
    
    def mark_approved(self, request, queryset):
        rows_updated = queryset.update(status='a')
        if rows_updated == 1:
            msg = "Marked 1 tag as approved"
        else:
            msg = "Marked %d tags as approved" % (rows_updated)
        self.message_user(request, msg)
    mark_approved.short_description = "Approve selected tags"
    
    def mark_disapproved(self, request, queryset):
        rows_updated = queryset.update(status='d')
        if rows_updated == 1:
            msg = "Marked 1 tag as disapproved"
        else:
            msg = "Marked %d tags as disapproved" % (rows_updated)
        self.message_user(request, msg)
    mark_disapproved.short_description = "Disapprove selected tags"
    
    def get_actions(self, request):
        actions = super(TagAdmin, self).get_actions(request)
        if not is_administrator(request.user):
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

class NoticeAdmin(admin.ModelAdmin):
    list_per_page = 30
    list_display = ('title', 'author', 'dateCreated', 'datePublished', 'dateExpired', 'status')
    list_filter = ['status', 'tags',]
    search_fields = ['title', 'author__username',] #format entry: <columnName>__<modelName>
    actions = ['mark_approved', 'mark_disapproved']
    
    fieldsets = [
        (None, {
            'classes': ('wide',),
            'fields': ['title', 'content', 'tags', 'status', 'datePublished', 'dateExpired', 'dateCreated', 'author', 'viewCount', 'originalNotice', 'image']
        })
    ]
    readonly_fields = ['dateCreated', 'originalNotice', 'author', 'viewCount']
    form = admin_notice_form
    
    def has_add_permission(self, request, obj=None):
        return False # Due to constraints we put in the admin site, admins and moderators have to go to the regular user interface to post notices.
        
    def mark_approved(self, request, queryset):
        rows_updated = queryset.update(status='a')
        if rows_updated == 1:
            msg = "Marked 1 notice as approved"
        else:
            msg = "Marked %d notices as approved" % (rows_updated)
        self.message_user(request, msg)
    mark_approved.short_description = "Approve selected notices"
    
    def mark_disapproved(self, request, queryset):
        rows_updated = queryset.update(status='d')
        if rows_updated == 1:
            msg = "Marked 1 notice as disapproved"
        else:
            msg = "Marked %d notices as disapproved" % (rows_updated)
        self.message_user(request, msg)
    mark_disapproved.short_description = "Disapprove selected notices"
    
    def get_actions(self, request):
        actions = super(NoticeAdmin, self).get_actions(request)
        if not is_administrator(request.user):
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

class N2UserAdmin(admin.ModelAdmin):
    list_per_page = 30
    list_display = ('username', 'first_name', 'last_name', 'email', 'status', 'is_active', 'can_post_notice_directly')
    list_filter = ['status',]
    search_fields = ['username',] #format entry: <columnName>__<modelName>
    actions = ['mark_approved', 'mark_disapproved']

    readonly_fields = ['last_login', 'date_joined']

    def has_add_permission(self, request, obj=None):
        return False # Due to constraints we put in the admin site, admins and moderators have to go to the regular user interface to post notices.

    def has_change_permission(self, request, obj=None):
        return is_moderator(request.user)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'user_permissions':
            if is_superuser(request.user):
                query = Q(codename__contains="")
            else:
                query = Q(codename="directly_post_notice") | Q(codename="add_notice") | Q(codename="change_notice")
            kwargs["queryset"] = Permission.objects.all().filter(query)
            kwargs["widget"] = django.forms.widgets.CheckboxSelectMultiple
            kwargs["help_text"] = ""
        return super(N2UserAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
    
    def get_form(self, request, obj=None, **kwargs):
        # Set the default fields for moderators
        self.fieldsets = [
            ('User details', {
                'classes': ('wide',),
                'fields': ('username', 'first_name', 'last_name', 'email',)
            }),
            ('User status', {
                'classes': ('wide',),
                'fields': ('status', 'is_active', 'user_permissions', 'last_login', 'date_joined',)
            }),
        ]
        
        # If the user is administrator, give additional privileges
        if is_administrator(request.user):
            self.fieldsets.append(
            ('Administrator section', {
                'classes': ('collapse', 'wide',),
                'fields': ('is_staff', 'groups', 'preferredTags', 'bookmarkedNotices', 'likedNotices', 'viewedNotices', 'hiddenNotices',)
            }))
        
        return super(N2UserAdmin, self).get_form(request, obj, **kwargs)
    
    
    def mark_approved(self, request, queryset):
        rows_updated = queryset.update(status='a')
        if rows_updated == 1:
            msg = "Marked %s as approved" % (queryset[0].username)
        else:
            msg = "Marked %d users as approved" % (rows_updated)
        self.message_user(request, msg)
    mark_approved.short_description = "Approve selected users"
    
    def mark_disapproved(self, request, queryset):
        rows_updated = queryset.update(status='d')
        if rows_updated == 1:
            msg = "Marked 1 user as disapproved"
        else:
            msg = "Marked %d users as disapproved" % (rows_updated)
        self.message_user(request, msg)
    mark_disapproved.short_description = "Disapprove selected users"

    def get_actions(self, request):
        actions = super(N2UserAdmin, self).get_actions(request)
        if not is_administrator(request.user):
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

from n2.moderator_site import moderator_site
moderator_site.register(TagGroup, TagGroupAdmin)
moderator_site.register(Tag, TagAdmin)
moderator_site.register(Notice, NoticeAdmin)
moderator_site.register(N2User, N2UserAdmin)