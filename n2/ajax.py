import json
from django.contrib.auth.decorators import login_required
from dajaxice.decorators import dajaxice_register
from n2.models import Notice


@dajaxice_register
def sayhello(request):
    return json.dumps({'message': 'hello. N2 here.'})

@dajaxice_register
@login_required()
def toggle_bookmark(request, notice_id):
    # remove
    if len(request.user.bookmarkedNotices.all().filter(pk=notice_id)) > 0:
        request.user.bookmarkedNotices.remove(Notice.objects.get(pk=notice_id))
        return json.dumps({'bookmark': 'removed'})
    # add
    else:
        request.user.bookmarkedNotices.add(Notice.objects.get(pk=notice_id))
        return json.dumps({'bookmark': 'added'})

@dajaxice_register
@login_required()
def toggle_like(request, notice_id):
    # remove
    if len(request.user.likedNotices.all().filter(pk=notice_id)) > 0:
        request.user.likedNotices.remove(Notice.objects.get(pk=notice_id))
        return json.dumps({'like': 'removed'})
    # add
    else:
        request.user.likedNotices.add(Notice.objects.get(pk=notice_id))
        return json.dumps({'like': 'added'})

@dajaxice_register
@login_required()
def toggle_hide(request, notice_id):
    # remove
    if len(request.user.hiddenNotices.all().filter(pk=notice_id)) > 0:
        request.user.hiddenNotices.remove(Notice.objects.get(pk=notice_id))
        return json.dumps({'hide': 'removed'})
    # add
    else:
        request.user.hiddenNotices.add(Notice.objects.get(pk=notice_id))
        return json.dumps({'hide': 'added'})
