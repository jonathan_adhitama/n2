from django.core.mail import send_mail
from django.contrib.auth import get_user_model
import socket

def get_ip():
	return socket.gethostbyname(socket.gethostname())

def send_activation(request):
	# Generate activation key
	newActivationKey = get_user_model().objects.make_random_password(length=30, allowed_chars='abcdefghijklmnopqrstuvwxyz0123456789')
	request.user.activationKey = newActivationKey
	request.user.save()

	subject = 'N2 Activation'
	link = "http://" + str(request.META['HTTP_HOST']) + "/activate/" + str(request.user.pk) + "/" + request.user.activationKey
	message = '''
	Welcome to N2. Please activate your account using the link below.

	%s

	Enjoy!
	''' %(link)
	mailfrom = 'N2'
	mailto = request.user.email
	send_mail(subject, message, mailfrom, [mailto], fail_silently=False)

def send_password_reset(request, user, password):
    subject = 'N2 Password Reset'
    message = '''
    Here's your new password: %s

    Don't forget to change it back!''' %(password)
    mailfrom = 'N2'
    mailto = user.email
    send_mail(subject, message, mailfrom, [mailto], fail_silently=False)