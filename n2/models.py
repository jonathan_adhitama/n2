import uuid
from django.db import models
from django.contrib.auth.models import UserManager, AbstractUser
from django.utils import timezone
from n2project import settings
import os

STATUS = (
    ('a', 'Approved'),
    ('d', 'Disapproved'),
    ('p', 'Pending'),
)

class TagGroup(models.Model):
    tagGroupName = models.CharField(max_length=100)
    
    tagGroupName.short_description = 'Tag Group Name'

    def __unicode__(self):
        return self.tagGroupName

class Tag(models.Model):
    tagGroup = models.ForeignKey(TagGroup)
    tagName = models.CharField(max_length=100)
    status = models.CharField(max_length=1, choices=STATUS)
    
    tagName.short_description = 'Tag Name'
    
    class Meta:
        permissions = (
            ('approve_tag', 'Approve new tags'),
        )

    def __unicode__(self):
        return self.tagName

class Notice(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=5000)
    dateCreated = models.DateTimeField()
    datePublished = models.DateTimeField()
    dateExpired = models.DateTimeField()
    status = models.CharField(max_length=1, choices=STATUS)
    viewCount = models.IntegerField()
    tags = models.ManyToManyField(Tag, blank=True)
    originalNotice = models.OneToOneField('Notice', blank=True, null=True, on_delete=models.SET_NULL)
    image = models.ImageField(upload_to="notices/"+str(uuid.uuid4()), blank=True, null=True)

    class Meta:
        permissions = (
            ('approve_notice', 'Approve new notice or modification to existing one'),
            ('directly_post_notice', 'Directly post new notice without being reviewed'),
        )

    def __unicode__(self):
        return self.title

    def num_views(self):
        return len(self.viewedBy.all())

    def num_likes(self):
        return len(self.likedBy.all())
    
    def is_published(self):
        return self.datePublished.toordinal() - timezone.now().toordinal() <= 0 and self.status == 'a'
    
    def is_expired(self):
        return self.dateExpired.toordinal() - timezone.now().toordinal() < 0

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(Notice, self).delete(*args, **kwargs)
        storage.delete(path)

class N2User(AbstractUser):
    status = models.CharField(max_length=1, choices=STATUS)
    preferredTags = models.ManyToManyField(Tag, blank=True, null=True)
    bookmarkedNotices = models.ManyToManyField(Notice, blank=True, null=True, related_name="bookmarkedBy")
    likedNotices = models.ManyToManyField(Notice, blank=True, null=True, related_name="likedBy")
    viewedNotices = models.ManyToManyField(Notice, blank=True, null=True, related_name="viewedBy")
    hiddenNotices = models.ManyToManyField(Notice, blank=True, null=True, related_name="hiddenBy")
    activationKey = models.CharField(max_length=50, blank=True, null=True)
    objects = UserManager()
    
    class Meta:
        permissions = (
            ('block_user', 'Block or unblock user'),
            ('grant_direct_post', 'Grant or un-grant a direct post permission'),
            ('reset_password', 'Reset other user\'s password'),
            ('change_user_group', 'Change user from/to moderator group, etc.'),
        )

    def __unicode__(self):
        return str(self.username)
    
    def can_post_notice_directly(self):
        return self.has_perm('n2.directly_post_notice')
    can_post_notice_directly.boolean = True
    can_post_notice_directly.short_description = 'Can post notice directly?'