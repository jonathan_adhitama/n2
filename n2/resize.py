from n2project import settings
from PIL import Image
import os

def resize_image(imageObject, size, outfile):
	newImage = imageObject.resize(size, Image.ANTIALIAS)
	newImage.save(outfile, "JPEG", quality=settings.QUALITY_VAL)

def resize(path_image):
    im = Image.open(path_image)
    width, height = im.size
    if ((width > settings.MAX_WIDTH) and (height > settings.MAX_HEIGHT)):
        ratio_width = float(width) / settings.MAX_WIDTH
        ratio_height = float(height) / settings.MAX_HEIGHT
        ratio = ratio_width if ratio_width >= ratio_height else ratio_height
        new_width = int(width / ratio)
        new_height = int(height / ratio)
        size = new_width, new_height
        resize_image(im, size, path_image)
    elif ((width > settings.MAX_WIDTH) and (height <= settings.MAX_HEIGHT)):
        ratio = float(width) / settings.MAX_WIDTH
        new_height = int(height / ratio)
        size = settings.MAX_WIDTH, new_height
        resize_image(im, size, path_image)
    elif ((width <= settings.MAX_WIDTH) and (height > settings.MAX_HEIGHT)):
        ratio = float(height) / settings.MAX_HEIGHT
        new_width = int(width / ratio)
        size = new_width, settings.MAX_HEIGHT
        resize_image(im, size, path_image)
    else:
        size = width, height
        resize_image(im, size, path_image)