from django.contrib.admin.sites import AdminSite

# Replacing autodiscover
import admin

class ModeratorSite(AdminSite):
	pass

moderator_site = ModeratorSite('moderator')