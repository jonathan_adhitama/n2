from datetime import datetime
from django import forms
from n2.models import Tag, TagGroup
from django.forms import extras
from n2project import settings

error_messages = {'required': 'Required',
                  'max_length': 'Too long',
                  'min_length': 'Too short',
                  'invalid': 'Invalid',
                  'invalid_choice': 'Invalid choice',
                  'invalid_list': 'Invalid list',
                  'missing': 'File is missing',
                  'empty': 'File is empty',
                  'invalid_image': 'Invalid image',
                  'max_value': 'Too big',
                  'min_value': 'Too small',
}

boolean_field_err_msg = {'required': 'Required',}

class login_form(forms.Form):
    username = forms.CharField(label="username", error_messages=error_messages)
    password = forms.CharField(widget=forms.PasswordInput, label="password", error_messages=error_messages)
    next = forms.CharField(widget=forms.HiddenInput, label="", error_messages=error_messages, required=False)

class register_form(forms.Form):
    username = forms.CharField(label="username", error_messages=error_messages)
    password = forms.CharField(widget=forms.PasswordInput, label="password", error_messages=error_messages)
    first_name = forms.CharField(label="first name", error_messages=error_messages)
    last_name = forms.CharField(label="last name", error_messages=error_messages)
    email = forms.EmailField(label="email", error_messages=error_messages)

def get_tags():
    all_tags = Tag.objects.filter(status='a')
    tag_choices = []
    for tag in all_tags:
        tag_choices.append((tag.pk, str(tag.tagGroup) + " - " + str(tag.tagName)))
    return tuple(tag_choices)

def get_all_tags():
    all_tags = []
    tag_groups = TagGroup.objects.all()
    for tag_group in tag_groups:
        tags = []
        for tag in tag_group.tag_set.filter(status='a'):
            tags.append({'pk': tag.pk, 'name': tag.tagName})
        all_tags.append({'group_name': tag_group.tagGroupName, 'tags': tags})
    return all_tags

class compose_form(forms.Form):
    title = forms.CharField(label="title", error_messages=error_messages)
    content = forms.CharField(label="content", widget=forms.Textarea, error_messages=error_messages)
    date_published = forms.DateField(label="date published", widget=extras.SelectDateWidget(), error_messages=error_messages)
    date_expired = forms.DateField(label="date expired", widget=extras.SelectDateWidget(), error_messages=error_messages)
    tags = forms.MultipleChoiceField(label="tags", widget=forms.CheckboxSelectMultiple, choices=get_tags(), required=False, error_messages=error_messages)
    image = forms.ImageField(label="image", required=False, error_messages=error_messages)
    original = forms.IntegerField(label="original", widget=forms.HiddenInput, initial=settings.NULL_PK, error_messages=error_messages)

def get_tag_groups():
    return [(tag_group.pk,tag_group.tagGroupName) for tag_group in TagGroup.objects.all()]
    
class add_tag_form(forms.Form):
    tag_name = forms.CharField(label='tag name', required=False, error_messages=error_messages)
    tag_group = forms.ChoiceField(label='tag group', choices=get_tag_groups(), required=False, error_messages=error_messages)
    
    def __init__(self, *args, **kwargs):
        super(add_tag_form, self).__init__(*args, **kwargs)
        self.fields['tag_name'].widget = forms.TextInput(attrs={'class':'tag_name_box'})
        self.fields['tag_group'].widget = forms.Select(attrs={'class':'tag_group_choice'})
        self.fields['tag_group'].choices = get_tag_groups()
        self.label_suffix = ''
    
    def as_table(self):
        from lxml import etree
        
        default = super(add_tag_form, self).as_table()
        added_root = '<root>'+default+'</root>'
        root = etree.fromstring(added_root)
        
        # Finds all input textfield tags
        inputs = []
        labels = []
        for i in root.iterdescendants():
            if i.tag == 'input' and 'type' in i.attrib and i.attrib['type'] == 'text':
                inputs.append(i)
            elif i.tag == 'label' and 'for' in i.attrib:
                labels.append(i)
        
        # Surround the found textfield input tags with the span tag
        for i in inputs:
            span = etree.Element('span')
            span.attrib['class'] = 'span_input_text span_input_text_create_tag'
            parent = i.getparent()
            parent.remove(i)
            parent.insert(0,span)
            span.insert(0,i)
        for l in labels:
            l.getparent().attrib['class'] = 'td_create_tag_label'
        
        # Return the modified tree back as string
        modified = ""
        for i in root:
            modified += etree.tostring(i, pretty_print=False)
        
        return modified

from django.forms.formsets import formset_factory
add_tag_formset = formset_factory(add_tag_form, extra=1)

class notice_search_form(forms.Form):
    keywords = forms.CharField(label="keywords", required=False, error_messages=error_messages)
    tags = forms.MultipleChoiceField(label="tags", widget=forms.CheckboxSelectMultiple, choices=get_tags(), required=False, error_messages=error_messages)
    show_hidden = forms.BooleanField(label="show_hidden", required=False, error_messages=error_messages)
    show_expired = forms.BooleanField(label="show_expired", required=False, error_messages=error_messages)
    bookmarked_only = forms.BooleanField(label="bookmarked_only", required=False, error_messages=error_messages)
    sort_choices = ( ('date_published', 'Recently published'),
                     ('num_views', 'Highest views'),
                     ('num_likes', 'Most liked'),
                   )
    sort_choice_default = 'date_published'
    sort_by = forms.ChoiceField(label="sort_by", widget=forms.RadioSelect, choices=sort_choices, initial=sort_choice_default, required=False, error_messages=error_messages)

    def __init__(self, *args, **kwargs):
        super(notice_search_form, self).__init__(*args, **kwargs)
        self.fields['tags'].choices = get_tags()
    
    
class user_form(forms.Form):
    username = forms.CharField(label="username", error_messages=error_messages)
    password = forms.CharField(widget=forms.PasswordInput, label="password", required=False, error_messages=error_messages)
    first_name = forms.CharField(label="first name", error_messages=error_messages)
    last_name = forms.CharField(label="last name", error_messages=error_messages)
    email = forms.EmailField(label="email", error_messages=error_messages)
    tags = forms.MultipleChoiceField(label="tags", widget=forms.CheckboxSelectMultiple, choices=get_tags(), required=False, error_messages=error_messages)

class file_form(forms.Form):
    uploaded = forms.ImageField(label='uploaded', error_messages=error_messages)

class admin_notice_form(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(admin_notice_form, self).__init__(*args, **kwargs)
        self.fields['content'].widget = forms.Textarea(attrs={'class':'vTextarea', 'cols':'50'})

class reset_password_form(forms.Form):
    username = forms.CharField(label="username", error_messages=error_messages)
    email = forms.EmailField(label="email", error_messages=error_messages)