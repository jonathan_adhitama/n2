from lxml.html.clean import Cleaner, autolink_html

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.utils.datetime_safe import datetime
from django.contrib import messages
from django.contrib.auth import logout, authenticate, login, get_user_model
from django.contrib.auth.decorators import login_required

from n2project import settings

from n2.email import send_activation, send_password_reset
from n2.forms import login_form, register_form, compose_form, get_all_tags, notice_search_form, user_form, reset_password_form, add_tag_formset
from n2.models import Notice, TagGroup, Tag, N2User
from n2.resize import resize
from n2.search import notice_search


def user_is_approved(user):
    return user.is_authenticated() and user.status == 'a'


@login_required()
def index(request):
    if not user_is_approved(request.user):
        return HttpResponseRedirect(reverse('n2:edit_user', args=()))

    # Check if user has permission to access index
    if not request.user.is_active:
        messages.add_message(request, messages.ERROR, "Oops. Your account is disabled. Please contact us.")
        return vlogout(request)
    
    # Setting up default values
    DEFAULT_KEYWORD = ""
    DEFAULT_SORT = "date_published"
    DEFAULT_REVERSE = True
    user_preferred_tags = [tag.pk for tag in request.user.preferredTags.all()]
    context = {'user': request.user,
               'tagGroups': TagGroup.objects.all(),
               'allUsers': get_user_model().objects.all(),
               'all_tags': get_all_tags(),
    }
    
    # Processing request
    try:
        form = notice_search_form(request.GET)
        
        # Determine the list of notices and the show/hide toggles based on form
        # values
        if request.GET.dict() == {}:
            # The user does not use the filter functionality.
            # Display based on default preferences.
            keywords = DEFAULT_KEYWORD
            tag_pks = [tag.pk for tag in request.user.preferredTags.all()]
            sort_by = DEFAULT_SORT
            show_hidden = False
            show_expired = False
            bookmarked_only = False
        elif form.is_valid():
            # The user submits a valid filter setting.
            keywords = form.cleaned_data['keywords']
            tag_pks = [int(i) for i in form.cleaned_data['tags']]
            sort_by = form.cleaned_data['sort_by']
            show_hidden = form.cleaned_data['show_hidden']
            show_expired = form.cleaned_data['show_expired']
            bookmarked_only = form.cleaned_data['bookmarked_only']
        else:
            # Notify user that the form is invalid
            err_msg = ""
            for i in form.errors:
                err_msg += i+": "+str(form.errors[i])+"\n"
            messages.add_message(request, messages.ERROR, 'Hmm, there was a problem with the search form. '+err_msg)
            
            keywords = DEFAULT_KEYWORD
            tag_pks = []
            sort_by = DEFAULT_SORT
            show_hidden = False
            show_expired = False
            bookmarked_only = False
        
        # Set the default sorting choice
        if sort_by == "":
            sort_by = notice_search_form.sort_choice_default
        
        # Fetch the notices list
        notices = notice_search(Notice.objects.all(), Tag.objects.all(), keywords, tag_pks, sort_by, True)
        
        # Further filter the notices
        # Hide notices not yet published
        notices = [notice for notice in notices if notice.is_published()]
        # Hide hidden notices, if necessary
        if not show_hidden:
            notices = [notice for notice in notices if len(request.user.hiddenNotices.filter(pk=notice.pk)) == 0]
        # Hide expired notices, if necessary
        if not show_expired:
            notices = [notice for notice in notices if not notice.is_expired()]
        # Select only bookmarked notices, if necessary
        if bookmarked_only:
            notices = [notice for notice in notices if len(request.user.bookmarkedNotices.filter(pk=notice.pk)) > 0]

        context['selected_tags'] = tag_pks
        context['notices'] = notices
        context['notice_search_form'] = notice_search_form(form.data)
        return render(request, "n2index.html", context)
    except Exception, e:
        messages.add_message(request, messages.ERROR, 'Oops, something bad happens :( '+str(e))
        return HttpResponseRedirect(reverse('n2:index'))


def vlogin(request):
    # Setting up default context variables
    context = {'login_form': login_form()}
    if 'next' in request.GET:
        context['next'] = request.GET['next']
        if not request.user.is_authenticated() and context['next'] != reverse('n2:index'):
            messages.add_message(request, messages.WARNING, 'Login required to proceed')
    
    if request.user.is_authenticated():
        if 'next' in context:
            messages.add_message(request, messages.ERROR, 'Oops. Something went wrong with authentication. Please contact us.')
        return HttpResponseRedirect(reverse('n2:index', args=()))
    elif request.method == 'POST':
        form = login_form(request.POST)
        # Set login_form context variable based on input values received
        context['login_form'] = login_form(form.data)
        
        # Preserving next_url
        if 'next' in request.POST:
            context['next'] = request.POST['next']
            next_url = request.POST['next']
        else:
            next_url = ""
        
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            
            user = authenticate(username=username, password=password)
            if user is not None:
                if not user.is_active:
                    messages.add_message(request, messages.ERROR, 'Oops. Your account is disabled. Please contact us.')
                    return HttpResponseRedirect(reverse('n2:login' ,args=()))
                else:
                    login(request, user)
                    if next_url == "":
                        return HttpResponseRedirect(reverse('n2:index', args=()))
                    else:
                        return HttpResponseRedirect(next_url)
            else:
                messages.add_message(request, messages.ERROR, "Oops. We can't log you in. Please check your username and password.")
                return render(request, 'n2login.html', context)
        else:
            messages.add_message(request, messages.ERROR, "Oops. We can't log you in. Please check your username and password.")
            return render(request, 'n2login.html', context)
    else:
        return render(request, 'n2login.html', context)


def vlogout(request):
    logout(request)
    return HttpResponseRedirect(reverse('n2:login', args=()))


def vregister(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('n2:index', args=()))
    else:
        if request.method == 'POST':
            form = register_form(request.POST)
            if form.is_valid():
                try:
                    # From form
                    username = form.cleaned_data['username']
                    password = form.cleaned_data['password']
                    first_name = form.cleaned_data['first_name']
                    last_name = form.cleaned_data['last_name']
                    email = form.cleaned_data['email']

                    # Extra checks
                    if len(get_user_model().objects.all().filter(username=username)) > 0:
                        raise Exception('Your selected username is already registered. Please select another username.')

                    if (len(email) < len(settings.ALLOWED_EMAIL_DOMAIN) + 2) or (
                            email[-len(settings.ALLOWED_EMAIL_DOMAIN):] != settings.ALLOWED_EMAIL_DOMAIN):
                        raise Exception(
                            'Please register with your university email address, which ends with ' + settings.ALLOWED_EMAIL_DOMAIN)

                    if len(get_user_model().objects.all().filter(email=email)) > 0:
                        raise Exception(
                            'Your provided email address is already registered. Please provide a different email address.')
                    
                    # To database
                    get_user_model().objects.create_user(
                        username=username,
                        password=password,
                        first_name=first_name,
                        last_name=last_name,
                        email=email,
                        status='p',
                    )

                    # Auth
                    user = authenticate(username=username, password=password)

                    # Login
                    login(request, user)

                    # Send activation key
                    send_activation(request)

                    return HttpResponseRedirect(reverse('n2:register_complete', args=()))
                except Exception, e:
                    messages.add_message(request, messages.ERROR, "Oops. We can't register you. " + str(e))
                    context = {'register_form': (register_form(form.data)),
                    }
                    return render(request, "n2register.html", context)
            else:
                messages.add_message(request, messages.ERROR, "Oops. We can't register you.")
                context = {'register_form': (register_form(form.data)),
                }
                return render(request, "n2register.html", context)
        else:
            context = {'register_form': (register_form()), }
            return render(request, "n2register.html", context)


@login_required()
def register_complete(request):
    return render(request, "n2registercomplete.html", {})


@login_required()
def notice(request, notice_id):
    if not user_is_approved(request.user):
        return HttpResponseRedirect(reverse('n2:edit_user', args=()))

    notice = get_object_or_404(Notice, pk=notice_id)
    notice.viewedBy.add(request.user)
    context = {'notice': notice,
               'notice_tags': notice.tags.all().filter(status='a'),
               'bookmarked': len(request.user.bookmarkedNotices.filter(pk=notice_id)) > 0,
               'liked': len(request.user.likedNotices.filter(pk=notice_id)) > 0,
               'hidden': len(request.user.hiddenNotices.filter(pk=notice_id)) > 0,
    }
    return render(request, "n2notice.html", context)


@login_required()
def compose(request, notice_id=settings.NULL_PK):
    if not user_is_approved(request.user):
        return HttpResponseRedirect(reverse('n2:edit_user', args=()))

    title_create_new = "create notice"
    title_edit = "editing "
    if request.method == 'POST':
        if int(request.POST['original']) == settings.NULL_PK:
            editor_title = title_create_new
        else:
            editor_title = title_edit + "\"" + Notice.objects.get(pk=request.POST['original']).title + "\""
        preferred_tags = [int(selected_tag_pk_u) for selected_tag_pk_u in request.POST.getlist('tags')]
        form = compose_form(request.POST, request.FILES)
        formset = add_tag_formset(request.POST)
        if form.is_valid() and formset.is_valid():
            try:
                # From form
                title = form.cleaned_data['title']
                content = form.cleaned_data['content']
                date_published = form.cleaned_data['date_published']
                date_expired = form.cleaned_data['date_expired']
                tags = form.cleaned_data['tags']
                original = form.cleaned_data['original']
                if 'image' in request.FILES:
                    image = request.FILES['image']
                    # Change extension to jpg to be further resized and compressed later
                    if str(request.FILES['image'].name.split('.')[-1]) != 'jpg':
                        request.FILES['image'].name = ".".join(request.FILES['image'].name.split('.')[:-1] + ['jpg'])
                else:
                    image = None
                if original == settings.NULL_PK:
                    original_notice = None
                else:
                    original_notice = Notice.objects.get(pk=original)

                # Make sure that date_expired > date_published
                if (date_expired < date_published) or (date_expired < timezone.now().date()):
                    raise Exception("Date Expired has to be later than both Date Published and today. ")
                
                # Creating any new tags from form
                for f in formset:
                    if ('tag_name' in f.cleaned_data and 'tag_group' in f.cleaned_data and
                        f.cleaned_data['tag_name'] != ""):
                        new_tag_name = f.cleaned_data['tag_name']
                        tag_group_id = f.cleaned_data['tag_group']
                        try:
                            tag_group_obj = TagGroup.objects.get(pk=tag_group_id)
                        except Exception, e:
                            messages.add_message(request, messages.ERROR, 'Can\'t create tag "%s".' % (new_tag_name))
                            continue
                        
                        new_tag = Tag.objects.create(
                            tagName=new_tag_name,
                            tagGroup=tag_group_obj,
                            status='p'
                        )
                        new_tag.save()
                        tags.append(new_tag.pk)
                        messages.add_message(request, messages.SUCCESS, 'Successfully added tag "%s"' % (new_tag_name))

                # Set the new notice status
                if request.user.has_perm('n2.directly_post_notice'):
                    status = 'a'
                else:
                    status = 'p'
                
                # To database
                cleaner = Cleaner(links=True, add_nofollow=True, host_whitelist=settings.LXML_CLEANER_HOST_WHITELIST)

                newNotice = Notice.objects.create(
                    author=request.user,
                    title=title,
                    content=autolink_html(cleaner.clean_html(content)),
                    dateCreated=datetime.now().date(),
                    datePublished=date_published,
                    dateExpired=date_expired,
                    status=status,
                    viewCount=0,
                    image=image,
                    originalNotice=original_notice,
                )
                for tag in tags:
                    newNotice.tags.add(tag)
                if 'image' in request.FILES:
                    resize(newNotice.image.path)

                if request.user.has_perm('n2.directly_post_notice'):
                    success_message = "Your notice \"%s\" has been added." %title
                else:
                    success_message = "Your notice \"%s\" has been submitted for approval." %title
                messages.add_message(request, messages.SUCCESS, success_message)

                return HttpResponseRedirect(reverse('n2:index', args=()))
            except Exception, e:
                messages.add_message(request, messages.ERROR, "Oops. There are still things to fix in this notice. " + str(e))
                context = {'all_tags': get_all_tags(),
                           'compose_form': (compose_form(form.data)),
                           'add_tag_formset': (add_tag_formset(formset.data)),
                           'editor_title': editor_title,
                           'preferred_tags': preferred_tags,
                }
                return render(request, "n2compose.html", context)
        else:
            messages.add_message(request, messages.ERROR, "Oops. There are still things to fix in this notice." + str(form.non_field_errors))
            context = {'all_tags': get_all_tags(),
                       'compose_form': (compose_form(form.data)),
                       'add_tag_formset': (add_tag_formset(formset.data)),
                       'editor_title': editor_title,
                       'preferred_tags': preferred_tags,
            }
            return render(request, "n2compose.html", context)
    else:
        if notice_id == settings.NULL_PK:
            editor_title = title_create_new
            composer_prefill = {'title': "",
                                'content': "",
                                'date_published': datetime.now(),
                                'date_expired': "",
            }
            preferred_tags = []
        else:
            original_notice = Notice.objects.get(pk=notice_id)
            editor_title = title_edit + "\"" + original_notice.title + "\""
            composer_prefill = {'title': "[Edited] " + original_notice.title,
                                'content': original_notice.content,
                                'date_published': original_notice.datePublished,
                                'date_expired': original_notice.dateExpired,
                                'original': notice_id,
            }
            preferred_tags = [tag.pk for tag in original_notice.tags.all()]
        context = {'compose_form': compose_form(initial=composer_prefill),
                   'add_tag_formset': (add_tag_formset()),
                   'all_tags': get_all_tags(),
                   'preferred_tags': preferred_tags,
                   'editor_title': editor_title,
        }
        return render(request, "n2compose.html", context)


def activate_user(request, user_pk, activation_key):
    try:
        user = get_user_model().objects.get(pk=user_pk)
        if user.activationKey == activation_key:
            user.status = 'a'
            user.save()
            messages.add_message(request, messages.SUCCESS, 'Welcome to N2! Your account is now active.')
            if request.user.is_authenticated() and request.user.pk != user_pk:
                logout(request)
        else:
            messages.add_message(request, messages.ERROR,
                                 'Oops. Something went wrong, we are unable to activate your account. Please contact us.')
    except Exception, e:
        messages.add_message(request, messages.ERROR,
                             'Oops. Something went wrong, we are unable to find your account. Please contact us.' + str(
                                 e))
    return HttpResponseRedirect(reverse('n2:index', args=()))


@login_required()
def edit_user(request):
    # Setting up default context variables
    preferred_tags = [tag.pk for tag in request.user.preferredTags.all()]
    prefilled_data = {'username': request.user.username,
                      'first_name': request.user.first_name,
                      'last_name': request.user.last_name,
                      'email': request.user.email}
    context = {'user_form': user_form(initial=prefilled_data),
               'preferred_tags': preferred_tags,
               'all_tags': get_all_tags()}
    
    if request.method == 'POST':
        form = user_form(request.POST)
        if form.is_valid():
            try:
                # From form
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                first_name = form.cleaned_data['first_name']
                last_name = form.cleaned_data['last_name']
                email = form.cleaned_data['email']

                tags = []
                if 'tags' in form.cleaned_data:
                    for tag_pk in form.cleaned_data['tags']:
                        try:
                            tags.append(Tag.objects.get(pk=tag_pk))
                        except Exception, e:
                            continue
                else:
                    tags = request.user.preferredTags.all()

                # Checking some stuff before saving user
                if username != request.user.username:
                    usernames = N2User.objects.all().filter(username = username)
                    if len(usernames) != 0:
                        raise Exception("username is taken")
                
                need_to_send_activation_email = False
                if email != request.user.email:
                    emails = N2User.objects.all().filter(email = email)
                    if len(emails) != 0:
                        raise Exception("email is used")
                    else:
                        # Sets the user status to pending for confirmation
                        # email
                        request.user.status = 'p'
                        need_to_send_activation_email = True

                # Saves data
                request.user.username = username
                if password != "":
                    request.user.set_password(password)
                request.user.first_name = first_name
                request.user.last_name = last_name
                request.user.email = email
                request.user.preferredTags.clear()
                for tag in tags:
                    request.user.preferredTags.add(tag)
                
                if need_to_send_activation_email:
                    send_activation(request)
                
                request.user.save()
                messages.add_message(request, messages.SUCCESS, 'Your profile has been updated.')
                
                # Display page appropriately
                return HttpResponseRedirect(reverse('n2:edit_user'))
            except Exception, e:
                messages.add_message(request, messages.ERROR, "Oops. We can't update your details. %s" % (e.message))#TODO: Want to delete the e.message part?
                context['user_form'] = user_form(form.data)
                return render(request, "n2edit_user.html", context)
        else:
            messages.add_message(request, messages.ERROR, "Oops. We can't update your details.")
            
            # Redisplay user input
            context['user_form'] = user_form(form.data)
            if 'tags' in form.cleaned_data:
                context['preferred_tags'] = [int(i) for i in form.cleaned_data['tags']]
            
            return render(request, "n2edit_user.html", context)
    else:
        if request.user.status == 'p':
            messages.add_message(request, messages.WARNING, 'Your account is not yet activated. Please check your inbox for an activation email.')
        elif request.user.status == 'd':
            messages.add_message(request, messages.ERROR, 'Your account is disabled. Please check your inbox for an activation email.')
        return render(request, "n2edit_user.html", context)


def reset_password(request):
    form = reset_password_form()
    if request.method == 'POST':
        form = reset_password_form(request.POST)
        if (not(form.is_valid())):
            messages.add_message(request, messages.ERROR, "Hmm, we can't verify that username and/or email address")
            return render(request, "n2reset.html", {'form' : form})
        
        # From form
        username_input = form.cleaned_data['username']
        email_input = form.cleaned_data['email']
        user = N2User.objects.all().filter(username=username_input)
        if (len(user) != 1):
            messages.add_message(request, messages.ERROR, "Hmm, we can't verify that username and/or email address")
            return render(request, "n2reset.html", {'form' : form})
        user = user[0]
        if(email_input != user.email):
            messages.add_message(request, messages.ERROR, "Hmm, we can't verify that username and/or email address")
            return render(request, "n2reset.html", {'form' : form})
        new_password = get_user_model().objects.make_random_password(length=10, allowed_chars='abcdefghijklmnopqrstuvwxyz0123456789')
        user.set_password(new_password)
        user.save()
        send_password_reset(request, user, new_password)
        messages.add_message(request, messages.ERROR, "Your new password has been emailed to your account. Please check your inbox.")
        return render(request, "n2reset.html", {'form' : form})
    return render(request, "n2reset.html", {'form' : form})


def about(request):
    return render(request, "n2about.html", {'logged_in': request.user.is_authenticated()})


def page403(request):
    messages.add_message(request, messages.ERROR, "Sorry, we are not able to show you the page you're looking for.")
    return HttpResponseRedirect(reverse('n2:index', args=()))


def page404(request):
    messages.add_message(request, messages.ERROR, "Sorry, we don't have the page you're looking for.")
    return HttpResponseRedirect(reverse('n2:index', args=()))


def page500(request):
    messages.add_message(request, messages.ERROR, "Sorry, something didn't work as expected. Please contact us and let us know.")
    return HttpResponseRedirect(reverse('n2:index', args=()))